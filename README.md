## Tanglescout

This is the official repository for the [**Tanglescout**](http://www.tanglescout.net), an IOTA token flow visualization and fund tracking platform currently in development.

The project is open source under GNU AGPLv3, feel free to contribute. There is a [tip jar right here](https://thetangle.org/address/NZWKACETQVBIPLBHQXBPOZ9BZXZWEETZEDEG9LSEVPQJROKHKONYZFFXOLAVWZGHHYW99NSIKEIASONXX), any proceeds will go directly into the development of the project!

We also have an [IOTA ecosystem page](https://ecosystem.iota.org/projects/tanglescout). For more information, you can contact Aconitin#1833 over on the [IOTA Discord](https://discordapp.com/channels/397872799483428865/397872799483428867)!