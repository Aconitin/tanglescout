//> Triggered on submitting an address!
function __Main_initialize(){

	//> Global Objects!
	Storage = new _Storage();		
	Search = new _Search();
	Renderer = new _Renderer();
	UI = new _UI();

	//> Enter the update loop!
	setInterval(__Main_update, 10);
	
//	Search.addStartingAddress("VOXYLLZOGHLEAWLIKUXT9BPLVHAOBYKXOTDIJMHHEGJODMTRGTCISKBYCBKOVKISNFZHC9RMHVSFAJ9XX");
	
};

//> Update!
function __Main_update(){

	Search.update();
	Storage.update();
	Renderer.update();
	
};