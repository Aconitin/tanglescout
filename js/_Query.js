//> Expands an address!
class _Query{

	//> Creation!
	constructor(hash, node="https://nodes.thetangle.org:443", direction="backward", zeroes=false, minRequestDelay=1, allowRequestBundling=true){
		
		//> Config and Variables!
		this.hash = hash;
		this.node = node;
		this.currentNode = new IOTA({"provider": this.node});
		this.zeroes = zeroes; //> Toggle ignore zero value transactions!
		this.direction = direction; //> Expanding direction of this query!
		this.hashes = []; //> Only bundle hashes!
		this.response = {};
		this.question = {"addresses": [this.hash]};
		this.status = "ready";
		this.out = [];
		this.minRequestDelay = minRequestDelay;
		this.timer = new _Timer(this.minRequestDelay);
		this.allowRequestBundling = allowRequestBundling;
		
		//> Debug print config!
		this.printVerbosity = 0; //> Minimum level that a message needs to have to get printed!
		this.enableDP = false;
		this.dp("Created new _Query instance with hash " + this.hash + "!", 5)
		
	}; //> END constructor!

	update(){
	
		this.timer.update();
		
		if (this.status == "ready"){ //> Ready to send query!
			if (this.timer.isDone()){
				this.ask();
				this.dp("Asked!", 0);
			};
			this.dp("Ready, waiting for timer ...", 0);
		} else if (this.status == "waiting"){ //> Waiting for response!
			//> Pass!
			this.dp("Waiting", 0);
		} else if (this.status == "received"){ //> Got a response, but haven't dealt with it yet!
			if (!this.response["error"]){
				this.dp("Received txos!", 0);
				this.timer.setDuration(Math.max(0.5 * this.timer.getDuration(), this.minRequestDelay));
				if ("addresses" in this.question){
					this.dp("Extraction Round ONE!", 0);
					this.extractTransactionsRoundOne();
				} else if ("bundles" in this.question){
					this.dp("Extraction Round TWO+", 0);
					this.extractTransactionsRoundTwo();
				};
				this.nextQuestion();
			} else { //> Node returned an error!
				this.dp("Received an ERROR!", 0);
				this.timer.setDuration(this.timer.getDuration() * 2);//> Set timeout higher!
				this.status = "ready";
				this.dp(this.response["error"], 99);
			};
			this.timer.reset();
		};
	};
	
	nextQuestion(){
		if (this.hashes.length == 0){
			this.status = "finished";
			this.dp("Finished!", 0);
		} else {
			if (this.allowRequestBundling == true){
				this.question = {"bundles": this.hashes};
				this.hashes = [];
			} else {
				this.question = {"bundles": [this.hashes.shift()]};
			};
			this.status = "ready";
			this.dp("Status set to ready!", 0);
		};
	};
	
	extractTransactionsRoundOne(){
		var bundles = [];
		this.response["txos"].forEach(function(txo){
			if (txo.value !=0 || this.zeroes == true){ //> Handle zero-value transactions!
				if (txo.value < 0 && this.direction == "forward"){ //> Handle direction!
					bundles.push(txo.bundle);
					this.out.push({"type":"transactionOUT", "hash":txo.hash, "from":txo.address, "to":txo.bundle, "value":txo.value, "timestamp":txo.timestamp});
				} else if (txo.value > 0 && this.direction == "backward"){
					bundles.push(txo.bundle);
					this.out.push({"type":"transactionIN", "hash":txo.hash, "from":txo.bundle, "to":txo.address, "value":txo.value, "timestamp":txo.timestamp});
				};
			};
		}.bind(this));
		bundles = [... new Set(bundles)];
		bundles.forEach(function(b){
			this.out.push({"type": "bundle", "hash": b});
			this.hashes.push(b);
		}.bind(this));
	};

	extractTransactionsRoundTwo(){
		this.response["txos"].forEach(function(txo){
			if (txo.value !=0 || this.zeroes == true){ //> Handle zero-value transactions!
				if (txo.value > 0 && this.direction == "forward"){ //> Handle direction!
					this.out.push({"type":"transactionIN", "hash":txo.hash, "from":txo.bundle, "to":txo.address, "value":txo.value, "timestamp":txo.timestamp});
					this.out.push({"type": "address", "hash": txo.address});
				} else if (txo.value < 0 && this.direction == "backward"){
					this.out.push({"type":"transactionOUT", "hash":txo.hash, "from":txo.address, "to":txo.bundle, "value":txo.value, "timestamp":txo.timestamp});
					this.out.push({"type": "address", "hash": txo.address});
				};
			};
		}.bind(this));
	};
	
	ask(){
		this.currentNode.api.findTransactionObjects(this.question, this.callback.bind(this));
		this.status = "waiting";
	};

	callback(error, txos){
		this.response["error"] = error;
		this.response["txos"] = txos;
		this.status = "received";
	};
	
	isResolveable(){
		return this.status == "finished";
	};
	
	getDirection(){
		return this.direction;
	};
	
	resolve(){
		this.status = "resolved";
		this.dp("_Query instance with hash " + this.hash + " resolved!");
		return this.out;
	};

	//> Debug Print!
	dp(txt, lvl=99){
		if(this.enableDP){
			if(lvl >= this.printVerbosity){
				console.log(txt);
			};
		};		
	};
	
}; //> END class!